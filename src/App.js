import React from 'react';
import Home from './pages/Home';
import Details from './pages/Details';

import {
  BrowserRouter as Router,
  Route
} from 'react-router-dom';

import AWSAppSyncClient from 'aws-appsync';
import { Rehydrated } from 'aws-appsync-react';
import { ApolloProvider } from 'react-apollo';
import appsyncConfig from './config/AppSync';

const appsyncClient = new AWSAppSyncClient({
  url: appsyncConfig.graphqlEndpoint,
  region: appsyncConfig.region,
  auth: { type: appsyncConfig.authenticationType, apiKey: appsyncConfig.apiKey }
});



const App = () => (
  <Router>
    <div>
      <Route exact path="/" component={Home}/>
      <Route path="/details" component={Details}/>
    </div>
  </Router>
)

const WithProvider = () => ( // 6
  <ApolloProvider client={appsyncClient}>
    <Rehydrated>
      <App />
    </Rehydrated>
  </ApolloProvider>
);

export default WithProvider;