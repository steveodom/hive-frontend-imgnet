export const directionColor = (direction) =>  {
  let color = "brown";
  if (direction === 'up') color = "green";
  if (direction === 'down') color = "red";
  return color;
};

export const directionIcon = (direction) =>  {
  let modifiedDirection = direction;
  if (direction === 'na') modifiedDirection = "na";
  return `trending_${modifiedDirection}`;
};