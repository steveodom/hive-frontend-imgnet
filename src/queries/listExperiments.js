import gql from 'graphql-tag';

export default gql(`
query {
  allImgNet {
    experiement
    date_number
    description
    accuracy
    model_type
    name
  }
}`);