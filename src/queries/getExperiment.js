import gql from 'graphql-tag';

export default gql(`
query GetImgNetExperiment($experiment: ID!, $date_number: Int!){
  getImgNet (
    experiement: $experiment, 
    date_number: $date_number,
  ) {
    experiement
    name
    date_number
    description
    accuracy
    model_type
    misses
    history
  }
}`);