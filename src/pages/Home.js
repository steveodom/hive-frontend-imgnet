import React, { Component } from 'react';
import Layout from './layout';

import ListOverview from '../containers/ListOverview';

import { Analytics } from 'aws-amplify';

import { Select } from 'rmwc/Select';
import { Grid, GridCell } from 'rmwc/Grid';
import "./../components/primitives/select.css";


export default class Home extends Component { 
  constructor(props) {
    super(props);
    this.state = {
      drawerOpenRight: false,
      modelType: "all"
    };
    this.handleOpenDrawer = this.handleOpenDrawer.bind(this);
  }

  handleOpenDrawer() {
    this.setState({drawerOpenRight: true});
    Analytics.record('right-menu-open');
  }

  componentDidMount() {
    Analytics.record('home-page-view');
  }

  render() {  
    return (
      <Layout title="Stock Pattern Recognition Through Deep Learning">    
        <div className="index--content"> 
          <Grid style={{padding: "0", paddingTop: "16px"}} className='content--narrow'>
            <GridCell span="2" style={{textAlign: "right"}}>
                <Select
                  label="Model Type"
                  className="griddetails--select"
                  onChange={evt => this.setState({modelType: evt.target.value})}
                  options={[
                    {
                      label: 'CNN',
                      value: 'cnn'
                    },
                    {
                      label: 'GANN',
                      value: 'gann'
                    }
                  ]}
                />
              </GridCell>
            </Grid>
            <ListOverview 
              handleOpenDrawer={this.handleOpenDrawer}
              modelType={this.state.modelType}
            />
        </div>       
      </Layout>
    )
  }
}