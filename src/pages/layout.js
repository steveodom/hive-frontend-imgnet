import React, { Component} from 'react';

import { Toolbar, ToolbarRow, ToolbarSection, ToolbarTitle } from 'rmwc/Toolbar';
import {Button} from 'rmwc/Button';
import MainMenu from '../components/layout/main-menu';

import '../../node_modules/material-components-web/dist/material-components-web.min.css';
import './layout.css';

import logo from './../images/icon.png';

import { Analytics } from 'aws-amplify';

export default class Layout extends Component {  
  constructor(props) {
    super(props);
    this.state = {drawerOpen: false};
    this.handleOpenDrawer = this.handleOpenDrawer.bind(this);
  }

  handleOpenDrawer() {
    this.setState({drawerOpen: true});
    Analytics.record('right-menu-open');
  }

  render() {
    const {title, children, menuIcon} = this.props;
    const {drawerOpen} = this.state;

    return (
      <div className='layout'>
      
        <Toolbar fixed>
          <ToolbarRow>
            <ToolbarSection alignStart theme={['text-primary-on-background']}>
              {menuIcon}
              <ToolbarTitle style={{paddingTop: '6px'}}>
                {!menuIcon && 
                  <a href="http://www.hiveresearch.com">
                    <img className="logo" src={logo} alt="Hive Research" /> 
                  </a>
                }
                {title}
              </ToolbarTitle>
            </ToolbarSection>
            <ToolbarSection alignEnd>
              <Button 
                className="layout--right-action"
                dense={true} 
                stroked 
                style={{color: "#aaa", borderColor: "#aaa"}}
                onClick={this.handleOpenDrawer}
              >
                Contact Us
              </Button>
            </ToolbarSection>
          </ToolbarRow>
        </Toolbar>
        <MainMenu open={drawerOpen}/>
        <div className="body--content">
          { children }
        </div>
      </div>
    )
  }
}