import React, { Component } from 'react';
import DetailsContainer from './../containers/DetailsContainer';

export default class Details extends Component { 
  constructor(props) {
    super(props);
    const params = new URLSearchParams(props.location.search);
    const experiment = params.get('experiment') || "";
    const date_number = parseInt(params.get('date_number'), 10) || 0;
    
    this.state = {
      experiment: experiment,
      date_number: date_number
    };
  }
  
  render() {    
    return (
      <DetailsContainer 
        experiment={this.state.experiment}
        date_number={this.state.date_number}
      />
    )
  }
}

