import React, { Component} from 'react';
import isempty from 'lodash.isempty';

import { GridList, GridTile, GridTilePrimary, GridTilePrimaryContent, GridTileSecondary, GridTileTitle, GridTileTitleSupportText } from 'rmwc/GridList';
import {Icon} from 'rmwc/Icon';

import Loader from './primitives/loader';
import Empty from './primitives/empty';

import {directionIcon, directionColor} from './../utils/formatter';

function Item(props) {
  const url = `https://s3.amazonaws.com/trading-logs-trades/charts/imgnet-misses/${props.experiment}/${props.pred}_${props.description}`;
  const predIcon = directionIcon(props.pred);
  const color = directionColor(props.actual);
  
  return (
    <GridTile>
				<GridTilePrimary>
					<GridTilePrimaryContent>
						<img 
              src={url} 
              alt={`${props.ticker}-chart`} 
              style={{top: "57px", height: "200px"}}
            />
					</GridTilePrimaryContent>
				</GridTilePrimary>
				<GridTileSecondary theme={['text-primary-on-dark']} style={{backgroundColor: color}}>
          <GridTileTitle>
            Actual: <strong>{props.actual}</strong>
          </GridTileTitle>
					<GridTileTitleSupportText>
            Prediction: <Icon>{predIcon}</Icon>
          </GridTileTitleSupportText>
				</GridTileSecondary>
			</GridTile>
  )
}

export default class GridDetails extends Component {
  
  render() {
    const {loading, items, experiment} = this.props;
    return (
      <div className="content--full">
        <GridList
            tileGutter1={false}
            headerCaption={true}
            twolineCaption={true}
            withIconAlignStart={false}
            tileAspect='3x4'
            style={{justifyContent: "center", alignItems: "center"}}
          >
          {loading && <Loader />}  
          {!loading && items && items.slice(0,50).map( (row, i) => {
            return <Item key={i} {...row} experiment={experiment}/>
          })}

          {!loading && isempty(items) && 
            <Empty>No misses found.</Empty>
          }
        </GridList>
      </div>
    )
  }
}
