import React from 'react';
import { Typography } from 'rmwc/Typography';
import { Grid, GridCell } from 'rmwc/Grid';

// http://jonjaques.github.io/react-loaders/
export default (props) => {
  return (
    <Grid>
      <GridCell span="12" style={{textAlign: "center"}}>
        <Typography use="subheading2">
          {props.children}
        </Typography>
      </GridCell>
    </Grid>
  )
}