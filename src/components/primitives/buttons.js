import React from 'react';
import {Button} from 'rmwc/Button';
import { Icon  } from "rmwc/Icon";
import {directionColor} from './../../utils/formatter';

export const FilterButton = (props) => {
  
  const isDense = props.dense || false;
  return (
    <Button 
      dense={isDense} 
      stroked 
      disabled={props.disabled}
      style={{color: props.color, borderColor: props.color}}
      onClick={props.handleClickOnFilter.bind(this, props.filter)}
    >
      {props.children}
    </Button>
  )
}
