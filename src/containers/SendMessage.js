import React, { Component} from 'react';
import * as EmailValidator from 'email-validator';
import { TextField } from 'rmwc/TextField';
import {Button} from 'rmwc/Button';
import {Typography} from 'rmwc/Typography';
import {sendToSlack} from './../utils/slack';

import './SendMessage.css';

export default class SendMessage extends Component {  

  constructor(props) {
    super(props);
    this.state = {email: '', message: '', status: false};

    this.handleEmailChange = this.handleEmailChange.bind(this);
    this.handleMessageChange = this.handleMessageChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleEmailChange(event) {
    this.setState({email: event.target.value});
  }

  handleMessageChange(event) {
    this.setState({message: event.target.value});
  }

  handleSubmit(event) {

    const {email, message} = this.state;
    event.preventDefault();
    sendToSlack(email, message).then( (res) => {
      if (res) {
        this.setState({status: "Message sent. We'll be in touch shortly."})
      } else {
        this.setState({status: "There was a system error. Try sending a message to steve.odom@gmail.com instead."})
      }
      setTimeout( () => {
        this.setState({status: false});
      }, 6000)
    })
  }

  isValid() {
    const {email, message} = this.state;
    return message.length > 2 && EmailValidator.validate(email);
  }

  render() {
    return (
      <div className="form--send-message">
        <form onSubmit={this.handleSubmit}>
          <TextField 
            fullwidth 
            validationmsg="An email address is required"
            label="your email" 
            value={this.state.email} 
            onChange={this.handleEmailChange}
          />
          <TextField 
            textarea 
            fullwidth 
            validationmsg="A message is required"
            label="type your message here"
            value={this.state.message} 
            onChange={this.handleMessageChange}
          />
          <Button unelevated disabled={!this.isValid()}>Send</Button>

          {this.state.status && 
            <div className="sendmessage--response">
              <Typography use="caption">{this.state.status}</Typography>
            </div>
          }
        </form>
      </div>
    )
  }
}