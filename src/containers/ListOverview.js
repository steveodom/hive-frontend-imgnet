import React, { Component} from 'react';
import { Link } from 'react-router-dom';
import isempty from 'lodash.isempty';

import Loader from './../components/primitives/loader';
import { 
  List, 
  ListItem, 
  ListItemText, 
  ListItemSecondaryText
} from 'rmwc/List';
import {Typography} from 'rmwc/Typography';
import Empty from './../components/primitives/empty';

import './list-overview.css';

import { graphql, compose } from 'react-apollo';
import ListExperiments from './../queries/listExperiments';

const modelColor = {
  "cnn": "#03A9F4",
  "gann": "#5D4037"
}
function Item(props) {  
  const accuracy = Math.round(props.accuracy * 100) / 100 * 100;
  return (
    <Link to={{ 
      pathname: '/details', 
      search: `?experiment=${props.experiement}&date_number=${props.date_number}`
    }}>
      <ListItem className="list-overview__list-item" id={props.id} ripple>
        <span className="list-overview__right-content">
          <span className="list-overview__right-icon" style={{"backgroundColor": modelColor[props.model_type]}}>
            {props.model_type}
          </span>
        </span>
        
        <ListItemText className="wrap list-overview__middle-content">
          <Typography use="title">
            {props.name}
          </Typography>
          <ListItemSecondaryText>
            {props.description}
          </ListItemSecondaryText>
        </ListItemText>
        <span className="list-overview__left-content">
          <Typography use="subheading2">{accuracy}%</Typography>
        </span>
        
      </ListItem>
    </Link>
  )
}

function HeaderItem(props) {
  return (
    <ListItem className="list-overview__list-item header" ripple>
      <span className="list-overview__right-label">Model Type</span>
      <span className="list-overview__middle-label">
        Name
      </span>
      <span className="list-overview__left-label">
        Accuracy
      </span>
    </ListItem>
  )
}

class ListOverview extends Component {
  filterResults() {
    const {results, modelType} = this.props;
    return results.filter( (item) => (item.model_type === modelType))
  }
  render() {
    const {results, loading, modelType} = this.props;

    const filteredResults = modelType === "all" ? results : this.filterResults()
    return (
      <div>
        <div className='content--narrow'>
          <List className="list-overview__list">
            <HeaderItem />
            {loading && <Loader />}  

            {!loading && isempty(filteredResults) && 
              <Empty>No experiments found.</Empty>
            }

            {!loading && filteredResults.map( (row, i) => {
              return <Item
                key={i} 
                {...row}
              />
            })}
          </List>
        </div>
      </div>
    )
  }
}

export default compose(
  graphql(
    ListExperiments,
    {
        options: {
            fetchPolicy: 'cache-and-network',
        },
        props: ({ 
          data: { 
            allImgNet = [],
            loading = true
          } 
        }) => ({
          results: allImgNet,
          loading: loading
        })
    }
  )
)(ListOverview);