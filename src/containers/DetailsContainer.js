import React, { Component } from 'react';

import Layout from './../pages/layout';
import Loader from './../components/primitives/loader';
import GridDetails from '../components/GridDetails';

import {Typography} from 'rmwc/Typography';
import { Select } from 'rmwc/Select';
import { Grid, GridCell } from 'rmwc/Grid';
import "./../components/primitives/select.css";

import { ToolbarMenuIcon } from 'rmwc/Toolbar';

import { Link } from 'react-router-dom';

import { Analytics } from 'aws-amplify';
import { graphql, compose } from 'react-apollo';

import {directionColor} from './../utils/formatter';
import GetExperiment from './../queries/getExperiment';


function BackButton(props) {
  return (
    <Link to={{ pathname: '/' }}>
      <ToolbarMenuIcon>arrow_back</ToolbarMenuIcon>
    </Link>
  )
}

class Details extends Component { 
  constructor(props) {
    super(props);
    
    this.state = { filter: "all" };
    this.handleSelectChange = this.handleSelectChange.bind(this);
  }

  componentDidMount() {
    Analytics.record('detail-page-view');
  }

  selectCombinations(parsedMisses) {
    const seen = {}
    parsedMisses.forEach( (miss) => {
      const value = `${miss.pred}-${miss.actual}`;
      const label = `Predicted ${miss.pred} but actually ${miss.actual}`;
      if (!seen[value]) { 
        const color = directionColor(miss.pred);
        seen[value] = {value, label, count: 1, color}
      } else {
        seen[value].count += 1;
      }
    });
    return Object.values(seen);
  }

  filterMisses(parsedMisses) {
    const {filter} = this.state;
    if (filter === "all") {
      return parsedMisses;
    } else {
      const [pred, actual] = filter.split('-');
      return parsedMisses.filter( (miss) => (miss.pred === pred && miss.actual === actual))
    }
  }

  handleSelectChange(evt) {
    let selected = evt.target.value;
    if (selected === this.state.filter) {
      selected = "all";
    }
    this.setState({filter: selected});
  }

  render() {
    const {name, description, misses, loading, experiement} = this.props;
    const parsedMisses = (misses && JSON.parse(misses)) || []
    const filteredMisses = this.filterMisses(parsedMisses)
    const selectCombinations = this.selectCombinations(parsedMisses)
    if (loading) return <Loader />;
    return (
      <Layout title="Validation Misses" menuIcon={<BackButton />}>  
          
          <div className="index--content">
            <Grid style={{paddingTop: "0"}}>
              <GridCell span="8">
                <Typography use="headline">{name}</Typography>
                <em><Typography use="body1">{description}</Typography></em>
              </GridCell>
              <GridCell span="4"></GridCell>
              <GridCell span="3" style={{textAlign: "right"}}>
                <Select
                  label="Filter by Miss Type:"
                  className="griddetails--select"
                  onChange={this.handleSelectChange}
                  options={selectCombinations}
                />
              </GridCell>
            </Grid>
            
            <GridDetails 
              loading={loading}
              items={filteredMisses || []}
              experiment={experiement}
            />
          </div>    
      </Layout>
    )
  }
}

export default compose(  
  graphql(
    GetExperiment,
    {
      options: (ownProps) => ({
          fetchPolicy: 'cache-and-network',
          variables: { 
            experiment: ownProps.experiment,
            date_number: parseInt(ownProps.date_number, 10)
          } 
      }),
      props: ({ 
        data: { 
          getImgNet = {},
          loading = true
        } 
      }) => ({
        ...getImgNet,
        loading: loading
      })
    }
  )
)(Details);